﻿namespace JBTestAssignment.Problems
{
    public class UndeclaredFunctionUsage : IProblem
    {
        public string FunctionName { get; }

        public UndeclaredFunctionUsage(string functionName)
        {
            FunctionName = functionName;
        }

        public string GetDescription()
        {
            return $"Function '{FunctionName}' is undefined";
        }
    }
}