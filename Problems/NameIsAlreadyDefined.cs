﻿namespace JBTestAssignment.Problems
{
    public class NameIsAlreadyDefined : IProblem
    {
        public string Name { get; }

        public NameIsAlreadyDefined(string name)
        {
            Name = name;
        }

        public string GetDescription()
        {
            return $"'{Name}' is already defined";
        }
    }
}