﻿namespace JBTestAssignment.Problems
{
    public class UndeclaredVariableUsage : IProblem
    {
        public string VariableName { get; }

        public UndeclaredVariableUsage(string variableName)
        {
            VariableName = variableName;
        }

        public string GetDescription()
        {
            return $"Variable '{VariableName}' is undefined";
        }
    }
}