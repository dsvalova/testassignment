﻿namespace JBTestAssignment.Problems
{
    public class UnassignedVariableUsage : IProblem
    {
        public string VariableName { get; }

        public UnassignedVariableUsage(string variableName)
        {
            VariableName = variableName;
        }

        public string GetDescription()
        {
            return $"Value of variable '{VariableName}' is undefined";
        }
    }
}