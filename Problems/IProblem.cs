﻿namespace JBTestAssignment.Problems
{
    public interface IProblem
    {
        string GetDescription();
    }
}