﻿using JBTestAssignment.Internal.Transformers;
using JBTestAssignment.Statements;
using JBTestAssignment.Tests.Helpers;
using NUnit.Framework;

namespace JBTestAssignment.Tests
{
    [TestFixture]
    internal class AlphaConverterShould
    {
        [Test]
        public void RenameSameEntitiesToSameNames()
        {
            var input = new Program
            {
                new VariableDeclaration("a"),
                new FunctionDeclaration("F"),
                new AssignVariable("a"),
                new PrintVariable("a"),
                new Invocation("F", false)
            };
            var expected = new[]{ "a1", "F1", "a1", "a1", "F1" };

            AssertThatAlphaConversionReturns(input, expected);
        }

        [Test]
        public void RenameDifferentEntitiesToDifferentNames()
        {
            var input = new Program
            {
                new FunctionDeclaration("F")
                {
                    FunctionBody =
                    {
                        new VariableDeclaration("a"),
                        new AssignVariable("a"),
                        new FunctionDeclaration("Local"),
                        new Invocation("G", false)
                    }
                },
                new FunctionDeclaration("G")
                {
                    FunctionBody =
                    {
                        new VariableDeclaration("a"),
                        new AssignVariable("a"),
                        new FunctionDeclaration("Local"),
                        new Invocation("Local", false)
                    }
                },
            };
            var expected = new[] { "F1", "a1", "a1", "Local1", "G1", "G1", "a2", "a2", "Local2", "Local2" };

            AssertThatAlphaConversionReturns(input, expected);
        }

        [Test]
        public void KeepNameCollisionAsIs()
        {
            var input = new Program
            {
                new FunctionDeclaration("F"),
                new FunctionDeclaration("F"),
                new VariableDeclaration("a"),
                new VariableDeclaration("a"),
            };
            var expected = new[] { "F1", "F1", "a1", "a1" };

            AssertThatAlphaConversionReturns(input, expected);
        }

        [Test]
        public void RenameUndeclaredEntities()
        {
            var input = new Program
            {
                new Invocation("F", false),
                new PrintVariable("a"),
                new AssignVariable("b"),
            };
            var expected = new[] { "F1", "a1", "b1" };

            AssertThatAlphaConversionReturns(input, expected);
        }

        [Test]
        public void RenameCapturedVariables()
        {
            var input = new Program
            {
                new AssignVariable("a"),
                new FunctionDeclaration("F")
                {
                    FunctionBody =
                    {
                        new PrintVariable("a"),
                    }
                }
            };
            var expected = new[] { "a1", "F1", "a1" };

            AssertThatAlphaConversionReturns(input, expected);
        }

        [Test]
        public void KeepImproperDeclarationAsIs()
        {
            var input = new Program
            {
                new PrintVariable("a"),
                new VariableDeclaration("a")
            };
            var expected = new[] { "a1", "a1"};

            AssertThatAlphaConversionReturns(input, expected);
        }

        [Test]
        public void RenameGlobalFunctionInSameName()
        {
            var input = new Program
            {
                new FunctionDeclaration("G")
                {
                    FunctionBody =
                    {
                        new FunctionDeclaration("H")
                        {
                            FunctionBody =
                            {
                                new Invocation("F", false)
                            }
                        }
                    }
                },
                new FunctionDeclaration("F")
            };
            var expected = new[] { "G1", "H1", "F1", "F1" };

            AssertThatAlphaConversionReturns(input, expected);
        }

        [Test]
        public void RenameGlobalVariableBeforeDeclarationInOtherContextInOtherName()
        {
            var input = new Program
            {
                new FunctionDeclaration("G")
                {
                    FunctionBody =
                    {
                        new PrintVariable("a")
                    }
                },
                new VariableDeclaration("a")
            };
            var expected = new[] { "G1", "a1", "a2" };

            AssertThatAlphaConversionReturns(input, expected);
        }

        private static void AssertThatAlphaConversionReturns(Program input, string[] expectedNames)
        {
            var nameGenerator = new NameWithNumberGenerator();
            var converter = new NameCollisionsResolver(nameGenerator.Generate);

            var newProgram = converter.Transform(input);

            var newNames = newProgram.GetAllNames();
            Assert.That(newNames, Is.EqualTo(expectedNames));
        }
    }
}
