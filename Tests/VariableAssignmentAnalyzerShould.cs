﻿using System;
using System.Linq;
using JBTestAssignment.Internal;
using JBTestAssignment.Problems;
using JBTestAssignment.Internal.Visitors;
using JBTestAssignment.Statements;
using JBTestAssignment.Tests.Helpers;
using NUnit.Framework;

namespace JBTestAssignment.Tests
{
    [TestFixture]
    internal class VariableAssignmentAnalyzerShould
    {
        [Category("Scale")]
        [Test, Timeout(250)]
        public void Scale()
        {
            /*
             *    var x;
             *    var y;
             *    y = ...;
             *    foo1();
             *    foo1();
             *    foo1();
             *    foo1();
             *    foo1();
             *    foo1();
             *    foo1();
             *    foo1();
             *    foo1();
             *    foo1();
             *    void foo1() {
             *      foo2();
             *      foo2();
             *      foo2();
             *      foo2();
             *      foo2();
             *      foo2();
             *      foo2();
             *      foo2();
             *      foo2();
             *    }
             *    void foo2() {
             *      foo3();
             *      foo3();
             *      foo3();
             *      foo3();
             *      foo3();
             *      foo3();
             *      foo3();
             *      foo3();
             *      foo3();
             *      foo3();
             *    }
             *    foo3() {
             *      foo4();
             *      foo4();
             *      foo4();
             *      foo4();
             *      foo4();
             *      foo4();
             *      foo4();
             *      foo4();
             *      foo4();
             *      foo4();
             *    }
             *    foo4() {
             *      foo5();
             *      foo5();
             *      foo5();
             *      foo5();
             *      foo5();
             *      foo5();
             *      foo5();
             *      foo5();
             *      foo5();
             *      foo5();
             *    }
             *    foo5() {
             *      foo6();
             *      foo6();
             *      foo6();
             *      foo6();
             *      foo6();
             *      foo6();
             *      foo6();
             *      foo6();
             *      foo6();
             *      foo6();
             *    }
             *    foo6() {
             *      foo7();
             *      foo7();
             *      foo7();
             *      foo7();
             *      foo7();
             *      foo7();
             *      foo7();
             *      foo7();
             *      foo7();
             *      foo7();
             *    }
             *    foo7() {
             *      x = ...;
             *      print(y);
             *    }
             *    print(x);
             */
            var input = new Program
            {
                new VariableDeclaration("x"),
                new VariableDeclaration("y"),
                new AssignVariable("y"),
                new Invocation("foo1", isConditional: false),
                new Invocation("foo1", isConditional: false),
                new Invocation("foo1", isConditional: false),
                new Invocation("foo1", isConditional: false),
                new Invocation("foo1", isConditional: false),
                new Invocation("foo1", isConditional: false),
                new Invocation("foo1", isConditional: false),
                new Invocation("foo1", isConditional: false),
                new Invocation("foo1", isConditional: false),
                new Invocation("foo1", isConditional: false),
                new FunctionDeclaration("foo1")
                {
                    FunctionBody =
                    {
                        new Invocation("foo2", isConditional: false),
                        new Invocation("foo2", isConditional: false),
                        new Invocation("foo2", isConditional: false),
                        new Invocation("foo2", isConditional: false),
                        new Invocation("foo2", isConditional: false),
                        new Invocation("foo2", isConditional: false),
                        new Invocation("foo2", isConditional: false),
                        new Invocation("foo2", isConditional: false),
                        new Invocation("foo2", isConditional: false),
                        new Invocation("foo2", isConditional: false),
                        new Invocation("foo2", isConditional: false),
                    }
                },
                new FunctionDeclaration("foo2")
                {
                    FunctionBody =
                    {
                        new Invocation("foo3", isConditional: false),
                        new Invocation("foo3", isConditional: false),
                        new Invocation("foo3", isConditional: false),
                        new Invocation("foo3", isConditional: false),
                        new Invocation("foo3", isConditional: false),
                        new Invocation("foo3", isConditional: false),
                        new Invocation("foo3", isConditional: false),
                        new Invocation("foo3", isConditional: false),
                        new Invocation("foo3", isConditional: false),
                        new Invocation("foo3", isConditional: false),
                        new Invocation("foo3", isConditional: false),
                    }
                },
                new FunctionDeclaration("foo3")
                {
                    FunctionBody =
                    {
                        new Invocation("foo4", isConditional: false),
                        new Invocation("foo4", isConditional: false),
                        new Invocation("foo4", isConditional: false),
                        new Invocation("foo4", isConditional: false),
                        new Invocation("foo4", isConditional: false),
                        new Invocation("foo4", isConditional: false),
                        new Invocation("foo4", isConditional: false),
                        new Invocation("foo4", isConditional: false),
                        new Invocation("foo4", isConditional: false),
                        new Invocation("foo4", isConditional: false),
                        new Invocation("foo4", isConditional: false),
                    }
                },
                new FunctionDeclaration("foo4")
                {
                    FunctionBody =
                    {
                        new Invocation("foo5", isConditional: false),
                        new Invocation("foo5", isConditional: false),
                        new Invocation("foo5", isConditional: false),
                        new Invocation("foo5", isConditional: false),
                        new Invocation("foo5", isConditional: false),
                        new Invocation("foo5", isConditional: false),
                        new Invocation("foo5", isConditional: false),
                        new Invocation("foo5", isConditional: false),
                        new Invocation("foo5", isConditional: false),
                        new Invocation("foo5", isConditional: false),
                        new Invocation("foo5", isConditional: false),
                    }
                },
                new FunctionDeclaration("foo5")
                {
                    FunctionBody =
                    {
                        new Invocation("foo6", isConditional: false),
                        new Invocation("foo6", isConditional: false),
                        new Invocation("foo6", isConditional: false),
                        new Invocation("foo6", isConditional: false),
                        new Invocation("foo6", isConditional: false),
                        new Invocation("foo6", isConditional: false),
                        new Invocation("foo6", isConditional: false),
                        new Invocation("foo6", isConditional: false),
                        new Invocation("foo6", isConditional: false),
                        new Invocation("foo6", isConditional: false),
                        new Invocation("foo6", isConditional: false),
                    }
                },
                new FunctionDeclaration("foo6")
                {
                    FunctionBody =
                    {
                        new Invocation("foo7", isConditional: false),
                        new Invocation("foo7", isConditional: false),
                        new Invocation("foo7", isConditional: false),
                        new Invocation("foo7", isConditional: false),
                        new Invocation("foo7", isConditional: false),
                        new Invocation("foo7", isConditional: false),
                        new Invocation("foo7", isConditional: false),
                        new Invocation("foo7", isConditional: false),
                        new Invocation("foo7", isConditional: false),
                        new Invocation("foo7", isConditional: false),
                        new Invocation("foo7", isConditional: false),
                    }
                },
                new FunctionDeclaration("foo7")
                {
                    FunctionBody =
                    {
                        new AssignVariable("x"),
                        new PrintVariable("y")
                    }
                },
                new PrintVariable("x")
            };

            var expectedErrors = Type.EmptyTypes;

            AnalyzerTests<VariableAssignmentAnalyzer>.ReturnErrors(input, expectedErrors);
        }

        private static readonly TestCaseData[] LeakyContextCases =
        {
            new TestCaseData(new Program
                {
                    /*
                     *    var x;
                     *    void foo1() {
                     *      void bar() {  foo2();  }
                     *      bar();
                     *    }
                     *    void foo2() {
                     *      bar();
                     *      void bar() {  x = ...;  }
                     *    }
                     *    foo1();
                     *    print(x);
                     */
                    new VariableDeclaration("x"),
                    new FunctionDeclaration("foo1")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("bar")
                            {
                                FunctionBody =
                                {
                                    new Invocation("foo2", isConditional: false)
                                }
                            },
                            new Invocation("bar", isConditional: false)
                        }
                    },
                    new FunctionDeclaration("foo2")
                    {
                        FunctionBody =
                        {
                            new Invocation("bar", isConditional: false),
                            new FunctionDeclaration("bar")
                            {
                                FunctionBody =
                                {
                                    new AssignVariable("x")
                                }
                            }
                        }
                    },
                    new Invocation("foo1", isConditional: false),
                    new PrintVariable("x")
                },
                Type.EmptyTypes)
                .SetName("LeakyContext_LocalCallIsNotARecursion"),
            new TestCaseData(new Program
                {
                    /*
                     *    var x;
                     *    void foo1() {
                     *      void bar() {  x = ...  }
                     *      foo2();
                     *    }
                     *    void foo2() {
                     *      bar();
                     *    }
                     *    foo1();
                     *    print(x);
                     */
                    new VariableDeclaration("x"),
                    new FunctionDeclaration("foo1")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("bar")
                            {
                                FunctionBody =
                                {
                                    new AssignVariable("x")
                                }
                            },
                            new Invocation("foo2", isConditional: false)
                        }
                    },
                    new FunctionDeclaration("foo2")
                    {
                        FunctionBody =
                        {
                            new Invocation("bar", isConditional: false),
                        }
                    },
                    new Invocation("foo1", isConditional: false),
                    new PrintVariable("x")
                },
                new []{ typeof(UnassignedVariableUsage) })
                .SetName("LeakyContext_UndeclaredFunctionIsNotARecursion"),
            new TestCaseData(new Program
                {
                    /*
                     *    void foo1() {
                     *      var x;
                     *      foo2();
                     *      print(x);
                     *    }
                     *    void foo2() {
                     *      x = ...;
                     *    }
                     *    foo1();
                     */
                    new FunctionDeclaration("foo1")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("x"),
                            new Invocation("foo2", isConditional: false),
                            new PrintVariable("x"),
                        }
                    },
                    new FunctionDeclaration("foo2")
                    {
                        FunctionBody =
                        {
                            new AssignVariable("x"),
                        }
                    },
                    new Invocation("foo1", isConditional: false)
                },
                new []{ typeof(UnassignedVariableUsage) })
                .SetName("LeakyContext_CantInitializeVariableFromOtherLocalContext"),
        };
        [TestCaseSource(nameof(LeakyContextCases))]
        public void LeakyContext(Program input, Type[] expectedErrors)
        {
            AnalyzerTests<VariableAssignmentAnalyzer>.ReturnErrors(input, expectedErrors);
        }

        private static readonly TestCaseData[] NoErrorsCases =
        {
            new TestCaseData(new Program
                {
                    new VariableDeclaration("name"),
                    new AssignVariable("name"),
                    new PrintVariable("name"),
                })
                .SetName("DoNotReturnError_PrintDeclaredAndAssignedVariable"),
            new TestCaseData(new Program
                {
                    new PrintVariable("name"),
                })
                .SetName("DoNotReturnError_UndeclaredVariableUsage"),
            new TestCaseData(new Program
                {
                    new VariableDeclaration("name"),
                    new VariableDeclaration("name"),
                    new AssignVariable("name"),
                    new PrintVariable("name"),
                })
                .SetName("DoNotReturnError_TwoVariablesWithSameName_OneIsAssigned"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("name"),
                        }
                    },
                    new FunctionDeclaration("B")
                    {
                        FunctionBody =
                        {
                            new PrintVariable("name"),
                        }
                    }
                })
                .SetName("DoNotReturnError_VariableUsage_InOtherScope"),
            new TestCaseData(new Program
                {
                    /*
                     *  var foo;
                     *  func Boo() {
                     *    foo = ...;
                     *  }
                     *  Boo();
                     *  Print(foo);
                     */
                    new VariableDeclaration("foo"),
                    new FunctionDeclaration("Boo")
                    {
                        FunctionBody =
                        {
                            new AssignVariable("foo"),
                        }
                    },
                    new Invocation("Boo", false),
                    new PrintVariable("foo"),
                })
                .SetName("DoNotReturnError_AssignVariableInLocalFunctionDeclaration"),
            new TestCaseData(new Program
                {
                    /*
                     * var a
                     * fun F(){
                     *     a = ...
                     *     Print(a)
                     * }
                     * if ...
                     *     F()
                     */
                    new VariableDeclaration("a"),
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new AssignVariable("a"),
                            new PrintVariable("a")
                        }
                    },
                    new Invocation("F", true),
                })
                .SetName("DoNotReturnError_ConditionalCall_VariableIsAssignedInIt"),
            new TestCaseData(new Program
                {
                    /*
                     * var a
                     * fun F(){
                     *     a = ...
                     * }
                     * F()
                     * if ...
                     *     F()
                     * Print(a);
                     */
                    new VariableDeclaration("a"),
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new AssignVariable("a"),
                        }
                    },
                    new Invocation("F", false),
                    new Invocation("F", true),
                    new PrintVariable("a")
                })
                .SetName("DoNotReturnError_ConditionalCall_VariableWasAssignedAnyWay"),
        };
        [TestCaseSource(nameof(NoErrorsCases))]
        public void DoNotReturnError(Program input)
        {
            AnalyzerTests<VariableAssignmentAnalyzer>.DoNotReturnError(input);
        }

        private static readonly TestCaseData[] SimpleOneErrorCases =
        {
            new TestCaseData(new Program
                {
                    new VariableDeclaration("a"),
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new PrintVariable("a"),
                        }
                    },
                    new Invocation("F", false),
                    new AssignVariable("a"),
                    new Invocation("F", false),
                })
                .Returns(typeof(UnassignedVariableUsage))
                .SetName("ReturnError_InvocationOfSameFunctionGivesDifferentResults"),
            new TestCaseData(new Program
                {
                    new VariableDeclaration("name"),
                    new PrintVariable("name"),
                })
                .Returns(typeof(UnassignedVariableUsage))
                .SetName("ReturnError_VariableIsNotInitialized"),
            new TestCaseData(new Program
                {
                    new VariableDeclaration("name"),
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new AssignVariable("name")
                        }
                    },
                    new PrintVariable("name"),
                })
                .Returns(typeof(UnassignedVariableUsage))
                .SetName("ReturnError_VariableIsAssignedInUnusedFunction"),
            new TestCaseData(new Program
                {
                    new VariableDeclaration("name"),
                    new PrintVariable("name"),
                    new AssignVariable("name"),
                })
                .Returns(typeof(UnassignedVariableUsage))
                .SetName("ReturnError_VariableIsAssignedAfterUsage"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("Foo")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("a"),
                            new AssignVariable("a"),
                            new PrintVariable("a")
                        }
                    },
                    new FunctionDeclaration("Boo")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("a"),
                            new PrintVariable("a")
                        }
                    },
                    new Invocation("Foo", false),
                    new Invocation("Boo", false),
                })
                .Returns(typeof(UnassignedVariableUsage))
                .SetName("ReturnError_VariablesWithSameNameInDifferentScopes_OneIsNotAssigned"),
            new TestCaseData(new Program
                {
                    /*
                     * var a
                     * fun F(){
                     *     a = ...
                     *     Print(a)
                     * }
                     * if ...
                     *     F()
                     * Print(a)
                     */
                    new VariableDeclaration("a"),
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new AssignVariable("a"),
                            new PrintVariable("a"),
                        }
                    },
                    new Invocation("F", true),
                    new PrintVariable("a"),
                })
                .Returns(typeof(UnassignedVariableUsage))
                .SetName("ReturnError_ConditionalCall_InternalPrintIsFine"),
            new TestCaseData(new Program
                {
                    /*
                     * var a;
                     * fun F(){
                     *     fun G(){
                     *         a = ...;
                     *     }
                     *     G();
                     *     Print(a);
                     * }
                     * if ...
                     *     F();
                     * Print(a);
                     */
                    new VariableDeclaration("a"),
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("G")
                            {
                                FunctionBody =
                                {
                                    new AssignVariable("a"),
                                }
                            },
                            new Invocation("G", false),
                            new PrintVariable("a"),
                        }
                    },
                    new Invocation("F", true),
                    new PrintVariable("a")
                })
                .Returns(typeof(UnassignedVariableUsage))
                .SetName("ReturnError_ConditionalCall_InternalCallWithInvocation"),
        };
        [TestCaseSource(nameof(SimpleOneErrorCases))]
        public Type ReturnError(Program input)
        {
            return AnalyzerTests<VariableAssignmentAnalyzer>.ReturnError(input);
        }

        private static readonly TestCaseData[] IgnoreCases =
        {
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("function")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("variable"),
                            new PrintVariable("variable"),
                        }
                    }
                })
                .SetName("Ignore_UsageOfUninitializedVariables_InUnusedFunctions"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("a"),
                            new AssignVariable("a"),
                            new PrintVariable("a"),
                        }
                    },
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("a"),
                            new PrintVariable("a"),
                        }
                    },
                    new Invocation("F", false),
                })
                .SetName("Ignore_SecondDeclarationOfFunction")
        };
        [TestCaseSource(nameof(IgnoreCases))]
        public void Ignore(Program input)
        {
            AnalyzerTests<VariableAssignmentAnalyzer>.DoNotReturnError(input);
        }

        private static readonly TestCaseData[] SeveralErrorsCases =
        {
            new TestCaseData(new Program
                {
                    new VariableDeclaration("a"),
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new PrintVariable("a"),
                        }
                    },

                    new Invocation("F", false),
                    new Invocation("F", true),
                })
                .SetName("ReturnTwoError_CallFunctionInBothWay"),
            new TestCaseData(new Program
                {
                    /*
                     * var a;
                     * fun F(){
                     *     fun G(){
                     *        a = ...;
                     *     }
                     *     if ...
                     *        G();
                     *     Print(a);
                     * }
                     * if ...
                     *     F();
                     * Print(a);
                     */
                    new VariableDeclaration("a"),
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("G")
                            {
                                FunctionBody =
                                {
                                    new AssignVariable("a"),
                                }
                            },
                            new Invocation("G", true),
                            new PrintVariable("a"),
                        }
                    },
                    new Invocation("F", true),
                    new PrintVariable("a")
                })
                .SetName("ReturnTwoError_ConditionalCalls"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("F"){
                        FunctionBody =
                        {
                            new VariableDeclaration("b"),
                            new PrintVariable("b"),
                        }
                    },
                    new Invocation("F", false),
                    new Invocation("F", false),
                })
                .SetName("ReturnTwoError_CallsNoConditions"),
        };
        [TestCaseSource(nameof(SeveralErrorsCases))]
        public void FindSeveralErrors(Program input)
        {
            var expectedErrors = new[] { typeof(UnassignedVariableUsage), typeof(UnassignedVariableUsage) };

            AnalyzerTests<VariableAssignmentAnalyzer>.ReturnErrors(input, expectedErrors);
        }
    }
}
