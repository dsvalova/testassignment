﻿using System;
using JBTestAssignment.Problems;
using JBTestAssignment.Internal.Visitors;
using JBTestAssignment.Statements;
using JBTestAssignment.Tests.Helpers;
using NUnit.Framework;

namespace JBTestAssignment.Tests
{
    [TestFixture]
    internal class NamesUniquenessAnalyzerShould
    {
        private static readonly TestCaseData[] NoErrorsCases =
        {
            new TestCaseData(new Program
                {
                    new VariableDeclaration("a"),
                    new VariableDeclaration("b"),
                })
                .SetName("DoNotReturnError_TwoDifferentVariables"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("Boo")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("foo"),
                        }
                    },
                    new FunctionDeclaration("Qxx")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("foo"),
                        }
                    },
                })
                .SetName("DoNotReturnErrors_SameVariableDeclaration_DifferentLocalFunctions"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("FirstName"),
                    new FunctionDeclaration("SecondName"),
                })
                .SetName("DoNotReturnError_AllFunctionNamesAreDifferent"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("FirstName")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("CommonName"),
                        }
                    },
                    new FunctionDeclaration("SecondName")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("CommonName"),
                        }
                    },
                })
                .SetName("DoNotReturnError_CommonFunctionsName_ButInDifferentScopes"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("FirstName")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("CommonName"),
                        }
                    },
                    new FunctionDeclaration("SecondName")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("CommonName"),
                        }
                    },
                })
                .SetName("DoNotReturnError_FunctionAndVariableWithSameName_ButInDifferentScopes"),
        };
        [TestCaseSource(nameof(NoErrorsCases))]
        public void DoNotReturnError(Program input)
        {
            AnalyzerTests<NamesUniquenessAnalyzer>.DoNotReturnError(input);
        }

        private static readonly TestCaseData[] SimpleOneErrorCases =
        {
            new TestCaseData(new Program
                {
                    new VariableDeclaration("name"),
                    new VariableDeclaration("other_name"),
                    new VariableDeclaration("name"),
                })
                .Returns(typeof(NameIsAlreadyDefined))
                .SetName("ReturnError_VariableDeclaration_VariableWithSameNameExists"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("name"),
                    new FunctionDeclaration("other_name"),
                    new FunctionDeclaration("name"),
                })
                .Returns(typeof(NameIsAlreadyDefined))
                .SetName("ReturnError_FunctionDeclaration_FunctionWithSameNameExists"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("name"),
                    new VariableDeclaration("name"),
                })
                .Returns(typeof(NameIsAlreadyDefined))
                .SetName("ReturnError_VariableDeclaration_FunctionWithSameNameExists"),
            new TestCaseData(new Program
                {
                    /*
                    *  func Boo() {
                    *    var foo;
                    *  }
                    *  var foo;
                    */
                    new FunctionDeclaration("Boo")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("foo"),
                        }
                    },
                    new VariableDeclaration("foo"),
                })
                .Returns(typeof(NameIsAlreadyDefined))
                .SetName("ReturnError_VariableDeclarationInLocalFunction_VariableIsDeclaredAfter"),
            new TestCaseData(new Program
                {
                    /*
                    *  var foo;
                    *  func Boo() {
                    *    var foo;
                    *  }
                    */
                    new VariableDeclaration("foo"),
                    new FunctionDeclaration("Boo")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("foo"),
                        }
                    },
                })
                .Returns(typeof(NameIsAlreadyDefined))
                .SetName("ReturnError_VariableDeclarationInLocalFunction_VariableIsDeclaredBefore"),
        };
        [TestCaseSource(nameof(SimpleOneErrorCases))]
        public Type ReturnError(Program input)
        {
            return AnalyzerTests<NamesUniquenessAnalyzer>.ReturnError(input);
        }

        [Test]
        public void FindSeveralErrors()
        {
            var input = new Program
            {
                new VariableDeclaration("b"),
                new FunctionDeclaration("b"),
                new VariableDeclaration("b"),
            };
            var expectedErrors = new[] { typeof(NameIsAlreadyDefined), typeof(NameIsAlreadyDefined) };

            AnalyzerTests<NamesUniquenessAnalyzer>.ReturnErrors(input, expectedErrors);
        }
    }
}
