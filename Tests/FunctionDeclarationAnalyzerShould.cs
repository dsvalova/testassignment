﻿using System;
using JBTestAssignment.Problems;
using JBTestAssignment.Internal.Visitors;
using JBTestAssignment.Statements;
using JBTestAssignment.Tests.Helpers;
using NUnit.Framework;

namespace JBTestAssignment.Tests
{
    [TestFixture]
    internal class FunctionDeclarationAnalyzerShould
    {
        private static readonly TestCaseData[] NoErrorsCases =
        {
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("Function"),
                    new Invocation("Function", false),
                })
                .SetName("DoNotReturnError_Invocation_FunctionIsDeclared"),
            new TestCaseData(new Program
                {
                    new Invocation("Function", false),
                    new FunctionDeclaration("Function"),
                })
                .SetName("DoNotReturnError_Invocation_FunctionIsDeclaredAfterInvocation"),
            new TestCaseData(new Program
                {
                    /*
                     * func Function() {
                     *      func LocalFunction() {
                     *          OtherLocalFunction();
                     *      }
                     *
                     *      func OtherLocalFunction() {
                     *          LocalFunction();
                     *      }
                     * }
                     */
                    new FunctionDeclaration("Function")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("LocalFunction")
                            {
                                FunctionBody =
                                {
                                    new Invocation("OtherLocalFunction", false)
                                }
                            },
                            new FunctionDeclaration("OtherLocalFunction")
                            {
                                FunctionBody =
                                {
                                    new Invocation("LocalFunction", false)
                                }
                            }
                        }
                    },
                })
                .SetName("DoNotReturnError_InvocationInLocalFunction"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("Boo")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("Bar"),
                            new Invocation("Bar", false),
                        }
                    },
                    new FunctionDeclaration("Qxx")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("Bar"),
                            new Invocation("Bar", false),
                        }
                    },
                })
                .SetName("DoNotReturnError_Invocation_FunctionsWithSameNamesInDifferentScopes"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("Foo"),
                    new FunctionDeclaration("Foo"),
                    new Invocation("Foo", false)
                })
                .SetName("DoNotReturnError_Invocation_FunctionIsDeclaredTwice"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("Foo"),
                    new VariableDeclaration("Foo"),
                    new Invocation("Foo", false)
                })
                .SetName("DoNotReturnError_FunctionIsDeclared_IgnoreVariableWithSameName"),
        };
        [TestCaseSource(nameof(NoErrorsCases))]
        public void DoNotReturnError(Program input)
        {
            AnalyzerTests<FunctionDeclarationAnalyzer>.DoNotReturnError(input);
        }

        private static readonly TestCaseData[] SimpleOneErrorCases =
        {
            new TestCaseData(new Program
                {
                    new VariableDeclaration("variable"),
                    new Invocation("variable", false),
                })
                .Returns(typeof(UndeclaredFunctionUsage))
                .SetName("ReturnError_CallFunction_WhichIsAVariable"),
            new TestCaseData(new Program
                {
                    new Invocation("F", false),
                })
                .Returns(typeof(UndeclaredFunctionUsage))
                .SetName("ReturnError_CallFunction_WhichIsUndeclared"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("Boo")
                    {
                        FunctionBody =
                        {
                            new FunctionDeclaration("Bar"),
                        }
                    },
                    new FunctionDeclaration("Qxx")
                    {
                        FunctionBody =
                        {
                            new Invocation("Bar", false),
                        }
                    },
                })
                .Returns(typeof(UndeclaredFunctionUsage))
                .SetName("ReturnError_Invocation_FunctionInDifferentLocalFunction"),
        };
        [TestCaseSource(nameof(SimpleOneErrorCases))]
        public Type ReturnError(Program input)
        {
            return AnalyzerTests<FunctionDeclarationAnalyzer>.ReturnError(input);
        }

        [Test]
        public void FindSeveralErrors()
        {
            var input = new Program
            {
                new Invocation("a", false),
                new Invocation("b", false),
            };
            var expectedErrors = new[] { typeof(UndeclaredFunctionUsage), typeof(UndeclaredFunctionUsage) };

            AnalyzerTests<FunctionDeclarationAnalyzer>.ReturnErrors(input, expectedErrors);
        }
    }
}
