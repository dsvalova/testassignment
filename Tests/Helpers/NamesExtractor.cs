﻿using System.Collections.Generic;
using System.Linq;
using JBTestAssignment.Internal.Visitors;
using JBTestAssignment.Statements;

namespace JBTestAssignment.Tests.Helpers
{
    internal static class TestExtensions
    {
        public static List<string> GetAllNames(this IEnumerable<IStatement> statements)
        {
            var extractor = new NamesExtractor();
            return statements.SelectMany(statement => statement.Accept(extractor)).ToList();
        }
    }

    internal class NamesExtractor : IVisitor<List<string>>
    {
        public List<string> Visit(VariableDeclaration statement)
        {
            return new List<string>{ statement.VariableName };
        }

        public List<string> Visit(AssignVariable statement)
        {
            return new List<string> { statement.VariableName };
        }

        public List<string> Visit(PrintVariable statement)
        {
            return new List<string> { statement.VariableName };
        }

        public List<string> Visit(FunctionDeclaration statement)
        {
            return new List<string>{ statement.FunctionName }
                .Concat(statement.FunctionBody.SelectMany(localStatement => localStatement.Accept(this)))
                .ToList();
        }

        public List<string> Visit(Invocation statement)
        {
            return new List<string> { statement.FunctionName };
        }
    }
}