﻿using System.Collections.Generic;
using System.Linq;
using JBTestAssignment.Internal;
using JBTestAssignment.Internal.Transformers;
using JBTestAssignment.Statements;
using NUnit.Framework;

namespace JBTestAssignment.Tests.Helpers
{
    internal class DummyResolver : INameResolver
    {
        public string Resolve(string alias)
        {
            return $"{alias}'";
        }
    }

    [TestFixture]
    internal class NameRestorerShould
    {
        [Test]
        public void ReplaceNames()
        {
            var nameResolver = new DummyResolver();
            var expected = new[]
            {
                "a'", "b'", "c'", "d'", "f'", "e'", "g'",
                "local_a'", "local_b'", "local_c'", "local_d'", "local_f'", "local_e'"
            };
            var program = new Program
            {
                new PrintVariable("a"),
                new AssignVariable("b"),
                new VariableDeclaration("c"),
                new Invocation("d", false),
                new Invocation("f", true),
                new FunctionDeclaration("e"),
                new FunctionDeclaration("g", new Program
                {
                    new PrintVariable("local_a"),
                    new AssignVariable("local_b"),
                    new VariableDeclaration("local_c"),
                    new Invocation("local_d", false),
                    new Invocation("local_f", true),
                    new FunctionDeclaration("local_e"),
                }),
            };

            var newProgram = new NameRestorer(nameResolver).VisitEach(program);

            var names = newProgram.GetAllNames();
            Assert.That(names, Is.EqualTo(expected));
        }
    }
}
