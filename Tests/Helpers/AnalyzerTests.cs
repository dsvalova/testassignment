﻿using System;
using System.Linq;
using JBTestAssignment.Internal;
using NUnit.Framework;

namespace JBTestAssignment.Tests.Helpers
{
    internal static class AnalyzerTests<TAnalyzer>
        where TAnalyzer : IAnalyzer, new()
    {
        public static void DoNotReturnError(Program input)
        {
            var analyzer = new TAnalyzer();

            var problems = analyzer.Analyze(input);

            Assert.That(problems.Count, Is.EqualTo(0));
        }

        public static Type ReturnError(Program input)
        {
            var analyzer = new TAnalyzer();

            var problems = analyzer.Analyze(input);

            Assert.That(problems.Count, Is.EqualTo(1));
            return problems.First().GetType();
        }

        public static void ReturnErrors(Program input, Type[] expectedErrors)
        {
            var analyzer = new TAnalyzer();

            var problems = analyzer.Analyze(input);

            var actualErrors = problems.Select(problem => problem.GetType());
            Assert.That(actualErrors, Is.EquivalentTo(expectedErrors));
        }
    }
}
