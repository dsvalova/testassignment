﻿using System.Linq;
using JBTestAssignment.Internal;
using JBTestAssignment.Internal.Transformers;
using JBTestAssignment.Statements;

namespace JBTestAssignment.Tests.Helpers
{
    internal class NameRestorer : ITransformer
    {
        private readonly INameResolver nameResolver;

        public NameRestorer(INameResolver nameResolver)
        {
            this.nameResolver = nameResolver;
        }

        public Program Transform(Program program)
        {
            return new Program(this.VisitEach(program));
        }

        public IStatement Visit(VariableDeclaration statement)
        {
            return new VariableDeclaration(nameResolver.Resolve(statement.VariableName));
        }

        public IStatement Visit(AssignVariable statement)
        {
            return new AssignVariable(nameResolver.Resolve(statement.VariableName));
        }

        public IStatement Visit(PrintVariable statement)
        {
            return new PrintVariable(nameResolver.Resolve(statement.VariableName));
        }

        public IStatement Visit(FunctionDeclaration declaration)
        {
            var newBody = declaration.FunctionBody.Select(statement => statement.Accept(this));
            return new FunctionDeclaration(nameResolver.Resolve(declaration.FunctionName), new Program(newBody));
        }

        public IStatement Visit(Invocation statement)
        {
            return new Invocation(nameResolver.Resolve(statement.FunctionName), statement.IsConditional);
        }
    }
}