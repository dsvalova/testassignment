﻿using System.Collections.Generic;

namespace JBTestAssignment.Tests.Helpers
{
    internal class NameWithNumberGenerator
    {
        private readonly Dictionary<string, int> namesCount = new Dictionary<string, int>();

        public string Generate(string name)
        {
            if (!namesCount.ContainsKey(name))
                namesCount[name] = 0;

            namesCount[name]++;
            return $"{name}{namesCount[name]}";
        }
    }
}