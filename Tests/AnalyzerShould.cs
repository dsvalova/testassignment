﻿using System;
using System.Linq;
using JBTestAssignment.Problems;
using JBTestAssignment.Statements;
using NUnit.Framework;

namespace JBTestAssignment.Tests
{
    [TestFixture]
    internal class AnalyzerShould
    {
        private static readonly TestCaseData[] PositiveCasesFromExample =
        {
            new TestCaseData(
                new Program
                {
                    /*
                     *  var foo;
                     *  foo = ...;
                     *  print(foo); // ok, initialized
                     */
                    new VariableDeclaration("foo"),
                    new AssignVariable("foo"),
                    new PrintVariable("foo")
                })
                .SetName("DoNotReturnErrors_PrintVariable_DeclaredAndAssignedVariable"),
            new TestCaseData(
                new Program
                {
                    /*
                     *  var foo;
                     *  func Boo() {
                     *    print(foo);
                     *  }
                     *  
                     *  Bar();
                     *  print(foo); // ok, initialized
                     *  
                     *  func Bar() {
                     *    foo = ...;
                     *    if (...) {
                     *      Boo(); // ok, 'foo' initialized
                     *    }
                     *  }
                     */
                    new VariableDeclaration("foo"),
                    new FunctionDeclaration("Boo")
                    {
                        FunctionBody =
                        {
                            new PrintVariable("foo")
                        }
                    },
                    new Invocation("Bar", false),
                    new PrintVariable("foo"),
                    new FunctionDeclaration("Bar")
                    {
                        FunctionBody =
                        {
                            new AssignVariable("foo"),
                            new Invocation("Boo", true)
                        }
                    }
                })
                .SetName("DoNotReturnErrors_FunctionDeclaration_LocalFunction"),
        };
        [TestCaseSource(nameof(PositiveCasesFromExample))]
        public void HandleExamplesCorrectly(Program input)
        {
            var actualAnswer = new Analyzer().Analyze(input);

            Assert.That(actualAnswer.IsSuccess, Is.True);
        }

        private static readonly TestCaseData[] SimpleOneErrorCases =
        {
        };
        [TestCaseSource(nameof(SimpleOneErrorCases))]
        public Type ReturnError(Program input)
        {
            var problems = new Analyzer().Analyze(input);

            Assert.That(problems.Count, Is.EqualTo(1));
            return problems.First().GetType();
        }

        private static readonly TestCaseData[] SimplePositiveCases =
        {
            new TestCaseData(
                    /*
                     *  Boo(); // ok, declared
                     *  var foo;
                     *  func Boo() {
                     *    foo = ...; // ok, declared
                     *  }
                     */
                    new Program
                    {
                        new Invocation("Boo", false),
                        new VariableDeclaration("foo"),
                        new FunctionDeclaration("Boo")
                        {
                            FunctionBody =
                            {
                                new AssignVariable("foo"),
                            }
                        },
                    })
                .SetName("DoNotReturnError_AssignVariableInLocalFunctionInvocation"),
        };
        [TestCaseSource(nameof(SimplePositiveCases))]
        public void DoNotReturnError(Program input)
        {
            var actualAnswer = new Analyzer().Analyze(input);

            Assert.That(actualAnswer.IsSuccess, Is.True);
        }

        private static readonly TestCaseData[] SeveralErrorsCases = new[]
        {
            new TestCaseData(
                new Program
                {
                    new VariableDeclaration("unassigned"),
                    new FunctionDeclaration("Function")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("Function"),
                            new PrintVariable("unassigned"),
                            new Invocation("UndefinedFunction", false)
                        }
                    },
                    new AssignVariable("undeclared"),
                    new Invocation("Function", false),
                },
                new[]
                {
                    typeof(UndeclaredVariableUsage),
                    typeof(UndeclaredFunctionUsage),
                    typeof(NameIsAlreadyDefined),
                    typeof(UnassignedVariableUsage)
                })
                .SetName("AllExistingErrors"),
        };
        [TestCaseSource(nameof(SeveralErrorsCases))]
        public void FindSeveralErrors(Program input, Type[] expectedErrors)
        {
            var actualAnswer = new Analyzer().Analyze(input);

            var actualTypes = actualAnswer.Select(problem => problem.GetType());
            Assert.That(actualTypes, Is.EquivalentTo(expectedErrors));
        }

        [Test]
        [Timeout(100)]
        public void DoNotLoop_RecursionCall()
        {
            var doubleFunctionDeclaration = new Program
            {
                new FunctionDeclaration("function")
                {
                    FunctionBody =
                    {
                        new VariableDeclaration("variable"),
                        new Invocation("function", true)
                    }
                },
                new Invocation("function", false),
            };

            var analyzerResult = new Analyzer().Analyze(doubleFunctionDeclaration);

            Assert.That(analyzerResult.IsSuccess, Is.True);
        }

        [Test]
        public void DoNotThrowStackOverflow_For500Nesting()
        {
            var program = GetProgramWithHighDeclarationNesting(500);

            var result = new Analyzer().Analyze(program);

            Assert.That(result.IsSuccess, Is.True);
        }

        private static Program GetProgramWithHighDeclarationNesting(int nesting)
        {
            var internalBody = new Program
            {
                new VariableDeclaration("var"),
                new PrintVariable("var")
            };
            var currentDeclaration = new FunctionDeclaration(Guid.NewGuid().ToString(), internalBody);

            for (var i = 0; i < nesting; ++i)
            {
                var currentBody = new Program
                {
                    currentDeclaration
                };
                currentDeclaration = new FunctionDeclaration(Guid.NewGuid().ToString(), currentBody);
            }

            return new Program
            {
                currentDeclaration,
                new Invocation(currentDeclaration.FunctionName, false)
            };
        }
    }
}
