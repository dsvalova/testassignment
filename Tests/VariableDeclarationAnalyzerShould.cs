﻿using System;
using JBTestAssignment.Problems;
using JBTestAssignment.Internal.Visitors;
using JBTestAssignment.Statements;
using JBTestAssignment.Tests.Helpers;
using NUnit.Framework;

namespace JBTestAssignment.Tests
{
    [TestFixture]
    internal class VariableDeclarationAnalyzerShould
    {
        private static readonly TestCaseData[] NoErrorsCases =
        {
            new TestCaseData(new Program
                {
                    new VariableDeclaration("a"),
                    new PrintVariable("a"),
                })
                .SetName("DoNotReturnError_PrintVariable_VariableIsDeclaredBefore"),
            new TestCaseData(new Program
                {
                    new VariableDeclaration("a"),
                    new AssignVariable("a"),
                })
                .SetName("DoNotReturnError_AssignVariable_VariableIsDeclaredBefore"),
            new TestCaseData(new Program
                {
                    new VariableDeclaration("a"),
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new AssignVariable("a"),
                        }
                    }
                })
                .SetName("DoNotReturnError_AssignVariableInLocalFunction_VariableIsDeclaredBefore"),
        };

        [TestCaseSource(nameof(NoErrorsCases))]
        public void DoNotReturnError(Program input)
        {
            AnalyzerTests<VariableDeclarationAnalyzer>.DoNotReturnError(input);
        }

        private static readonly TestCaseData[] SimpleOneErrorCases =
        {
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("F")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("name"),
                        }
                    },
                    new FunctionDeclaration("B")
                    {
                        FunctionBody =
                        {
                            new PrintVariable("name"),
                        }
                    }
                })
                .Returns(typeof(UndeclaredVariableUsage))
                .SetName("ReturnError_VariableUsage_InOtherScope"),
            new TestCaseData(new Program
                {
                    new VariableDeclaration("name"),
                    new AssignVariable("other_name"),
                })
                .Returns(typeof(UndeclaredVariableUsage))
                .SetName("ReturnError_AssignVariable_VariableIsNotDeclared"),
            new TestCaseData(new Program
                {
                    new VariableDeclaration("name"),
                    new PrintVariable("other_name"),
                })
                .Returns(typeof(UndeclaredVariableUsage))
                .SetName("ReturnError_PrintVariable_VariableIsNotDeclared"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("function"),
                    new PrintVariable("function"),
                })
                .Returns(typeof(UndeclaredVariableUsage))
                .SetName("ReturnError_PrintVariable_WhichIsAFunction"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("function"),
                    new AssignVariable("function"),
                })
                .Returns(typeof(UndeclaredVariableUsage))
                .SetName("ReturnError_AssignVariable_WhichIsAFunction"),
            new TestCaseData(new Program
                {
                    new FunctionDeclaration("name")
                    {
                        FunctionBody =
                        {
                            new AssignVariable("name")
                        }
                    },
                })
                .Returns(typeof(UndeclaredVariableUsage))
                .SetName("ReturnError_FromUnusedFunction"),
            new TestCaseData(new Program
                {
                    /*
                    *  func Boo() {
                    *    foo = ...;
                    *  }
                    *  var foo;
                    */
                    new FunctionDeclaration("Boo")
                    {
                        FunctionBody =
                        {
                            new AssignVariable("foo"),
                        }
                    },
                    new VariableDeclaration("foo"),
                })
                .Returns(typeof(UndeclaredVariableUsage))
                .SetName("ReturnError_AssignDeclarationInLocalFunction_BeforeDeclaration"),
            new TestCaseData(new Program
                {
                    /*
                    *  func Boo() {
                    *    var foo;
                    *  }
                    *  foo = ...;
                    */
                    new FunctionDeclaration("Boo")
                    {
                        FunctionBody =
                        {
                            new VariableDeclaration("foo"),
                        }
                    },
                    new AssignVariable("foo"),
                })
                .Returns(typeof(UndeclaredVariableUsage))
                .SetName("ReturnError_AssignVariable_WhichIsDeclaredInLocalFunction"),
            new TestCaseData(new Program
                {
                    new PrintVariable("foo"),
                    new VariableDeclaration("foo"),
                })
                .Returns(typeof(UndeclaredVariableUsage))
                .SetName("ReturnError_PrintVariable_BeforeDeclaration"),
        };

        [TestCaseSource(nameof(SimpleOneErrorCases))]
        public Type ReturnError(Program input)
        {
            return AnalyzerTests<VariableDeclarationAnalyzer>.ReturnError(input);
        }

        [Test]
        public void FindSeveralErrors()
        {
            var input = new Program
            {
                new AssignVariable("a"),
                new PrintVariable("b"),
            };
            var expectedErrors = new[] { typeof(UndeclaredVariableUsage), typeof(UndeclaredVariableUsage) };

            AnalyzerTests<VariableDeclarationAnalyzer>.ReturnErrors(input, expectedErrors);
        }
    }
}