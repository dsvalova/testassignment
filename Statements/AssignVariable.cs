﻿using JBTestAssignment.Internal.Visitors;

namespace JBTestAssignment.Statements
{
    public class AssignVariable : IStatement
    {
        public AssignVariable(string variableName)
        {
            VariableName = variableName;
        }

        public string VariableName { get; }

        public override string ToString()
        {
            return $"{VariableName} = smth;";
        }

        public T Accept<T>(IVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}