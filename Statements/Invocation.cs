﻿using JBTestAssignment.Internal.Visitors;

namespace JBTestAssignment.Statements
{
    public class Invocation : IStatement
    {
        public Invocation(string functionName, bool isConditional)
        {
            FunctionName = functionName;
            IsConditional = isConditional;
        }

        public string FunctionName { get; }
        public bool IsConditional { get; }

        public override string ToString()
        {
            if (IsConditional)
                return $"if (smth) {FunctionName}();";

            return $"{FunctionName}();";
        }

        public T Accept<T>(IVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}