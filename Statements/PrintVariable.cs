﻿using JBTestAssignment.Internal.Visitors;

namespace JBTestAssignment.Statements
{
    public class PrintVariable : IStatement
    {
        public PrintVariable(string variableName)
        {
            VariableName = variableName;
        }

        public string VariableName { get; }

        public override string ToString()
        {
            return $"print({VariableName});";
        }

        public T Accept<T>(IVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}