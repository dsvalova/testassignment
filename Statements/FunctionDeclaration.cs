﻿using System.Text;
using JBTestAssignment.Internal.Visitors;

namespace JBTestAssignment.Statements
{
    public class FunctionDeclaration : IStatement
    {
        public FunctionDeclaration(string functionName)
        {
            FunctionName = functionName;
            FunctionBody = new Program();
        }

        public FunctionDeclaration(string functionName, Program body)
        {
            FunctionName = functionName;
            FunctionBody = body;
        }

        public string FunctionName { get; }
        public Program FunctionBody { get; }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("func ").Append(FunctionName).AppendLine(" {");
            builder.Append(FunctionBody);
            builder.Append('}');

            return builder.ToString();
        }

        public T Accept<T>(IVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}