﻿using JBTestAssignment.Internal.Visitors;

namespace JBTestAssignment.Statements
{
    public class VariableDeclaration : IStatement
    {
        public VariableDeclaration(string variableName)
        {
            VariableName = variableName;
        }

        public string VariableName { get; }

        public override string ToString()
        {
            return $"var {VariableName};";
        }

        public T Accept<T>(IVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}