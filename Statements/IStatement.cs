﻿using JBTestAssignment.Internal.Visitors;

namespace JBTestAssignment.Statements
{
    public interface IStatement
    {
        T Accept<T>(IVisitor<T> visitor);
    }
}