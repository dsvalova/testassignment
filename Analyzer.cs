﻿using System.Linq;
using JBTestAssignment.Internal;
using JBTestAssignment.Internal.Visitors;

namespace JBTestAssignment
{
    public class Analyzer
    {
        public AnalyzerResult Analyze(Program program)
        {
            var analyzers = new IAnalyzer[]
            {
                new FunctionDeclarationAnalyzer(),
                new VariableDeclarationAnalyzer(),
                new NamesUniquenessAnalyzer(),
                new VariableAssignmentAnalyzer(),
            };

            var problems = analyzers
                .AsParallel()
                .AsUnordered()
                .SelectMany(analyzer => analyzer.Analyze(program));
            return new AnalyzerResult(problems);
        }
    }
}