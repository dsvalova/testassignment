﻿using System.Collections.Generic;
using System.Linq;
using JBTestAssignment.Problems;

namespace JBTestAssignment
{
    public class AnalyzerResult : List<IProblem>
    {
        public bool IsSuccess => Count == 0;

        public AnalyzerResult(IEnumerable<IProblem> problems) : base(problems) { }

        public static readonly AnalyzerResult Successful = new AnalyzerResult(new List<IProblem>());

        public override string ToString()
        {
            if (IsSuccess)
                return "Success";

            var problems = this.Select(problem => problem.ToString());
            return $"Failed in:\n\t{string.Join("\n\t", problems)}";
        }
    }
}