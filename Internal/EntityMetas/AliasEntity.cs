﻿namespace JBTestAssignment.Internal.EntityMetas
{
    internal class AliasEntity : IEntityMeta
    {
        public string Alias { get; }

        public AliasEntity(string alias)
        {
            Alias = alias;
        }
    }
}