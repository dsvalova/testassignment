﻿using System;
using System.Collections.Generic;
using JBTestAssignment.Problems;

namespace JBTestAssignment.Internal.EntityMetas
{
    internal class FunctionMeta : IEntityMeta
    {
        public Program Body { get; set; }
        public bool HasBeenInvoked { get; set; }

        private AssignmentAnalysisResult assignmentAnalysisResult;

        public AssignmentAnalysisResult AssignmentAnalysisResult
        {
            get
            {
                EnsureHasBeenInvokedIs(true, $"Can't read '{nameof(AssignmentAnalysisResult)}' when '{nameof(HasBeenInvoked)}' is 'false'");
                return assignmentAnalysisResult;
            }
            set
            {
                EnsureHasBeenInvokedIs(false, $"Can't set '{nameof(AssignmentAnalysisResult)}' when '{nameof(HasBeenInvoked)}' is 'true'");
                assignmentAnalysisResult = value;
            }
        }

        private void EnsureHasBeenInvokedIs(bool value, string errorMessage)
        {
            if (HasBeenInvoked != value)
                throw new Exception(errorMessage);
        }
    }

    internal class AssignmentAnalysisResult
    {
        public readonly List<string> AssignedCapturedVariables;
        public readonly List<string> UsedCapturedVariables;
        public readonly List<IProblem> LocalProblems;

        public AssignmentAnalysisResult(
            List<string> assignedCapturedVariables,
            List<string> usedCapturedVariables,
            List<IProblem> localProblems)
        {
            AssignedCapturedVariables = assignedCapturedVariables;
            UsedCapturedVariables = usedCapturedVariables;
            LocalProblems = localProblems;
        }
    }
}