﻿namespace JBTestAssignment.Internal.EntityMetas
{
    internal class VariableMeta : IEntityMeta
    {
        public bool IsAssigned { get; set; }

        public override string ToString()
        {
            return $"{nameof(IsAssigned)} = '{IsAssigned}'";
        }
    }
}