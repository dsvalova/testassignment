﻿using System.Collections.Generic;
using JBTestAssignment.Internal.EntityMetas;

namespace JBTestAssignment.Internal.States
{
    internal class VariableAssignmentAnalyzerState : AnalyzerState
    {
        private readonly Stack<List<(string name, bool isAssigned)>> previousVariablesState;
        private readonly Stack<VariablesContext> contexts;

        public VariablesContext CurrentContext => contexts.Peek();

        public VariableAssignmentAnalyzerState()
        {
            previousVariablesState = new Stack<List<(string, bool)>>();

            contexts = new Stack<VariablesContext>();
            contexts.Push(new VariablesContext());
        }

        public override void EnterFunction(string functionName)
        {
            base.EnterFunction(functionName);

            previousVariablesState.Push(new List<(string name, bool isAssigned)>());
            contexts.Push(new VariablesContext());
        }

        public override void ExitFunction(string functionName)
        {
            contexts.Pop();
            RestorePreviousVariablesState();

            base.ExitFunction(functionName);
        }

        public void AddFunction(string name, FunctionMeta meta)
        {
            AddNamedEntity(name, meta);
        }

        public void AddVariable(string name, VariableMeta meta)
        {
            CurrentContext.AddLocal(name);
            AddNamedEntity(name, meta);
        }

        public void AssignVariableIfExists(string variableName)
        {
            if (TryGetVariable(variableName, out var variableMeta))
            {
                SaveVariableState(variableName, variableMeta.IsAssigned);
                variableMeta.IsAssigned = true;
            }

            if (!CurrentContext.HasLocal(variableName))
            {
                CurrentContext.AddCapturedVariableAssignment(variableName);
            }
        }

        public bool TryGetFunction(string name, out FunctionMeta meta)
        {
            return TryGetNamedEntity(name, out meta);
        }

        public bool TryGetVariable(string name, out VariableMeta meta)
        {
            return TryGetNamedEntity(name, out meta);
        }

        private void SaveVariableState(string name, bool isAssigned)
        {
            previousVariablesState.Peek().Add((name, isAssigned));
        }

        private void RestorePreviousVariablesState()
        {
            foreach (var (name, isAssigned) in previousVariablesState.Peek())
            {
                if (TryGetVariable(name, out var variableMeta))
                    variableMeta.IsAssigned = isAssigned;
            }

            previousVariablesState.Pop();
        }
    }
}