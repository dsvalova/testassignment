﻿using System.Collections.Generic;
using JBTestAssignment.Internal.EntityMetas;

namespace JBTestAssignment.Internal.States
{
    internal class AnalyzerState
    {
        private readonly HashSet<string> callSet;
        private readonly Stack<List<string>> scopes;
        private readonly Dictionary<string, IEntityMeta> namedEntities;

        public AnalyzerState()
        {
            callSet = new HashSet<string>();
            namedEntities = new Dictionary<string, IEntityMeta>();
            scopes = new Stack<List<string>>();
            scopes.Push(new List<string>());
        }

        public virtual void EnterFunction(string functionName)
        {
            callSet.Add(functionName);
            scopes.Push(new List<string>());
        }

        public virtual void ExitFunction(string functionName)
        {
            callSet.Remove(functionName);
            foreach (var name in scopes.Peek())
            {
                namedEntities.Remove(name);
            }

            scopes.Pop();
        }

        public bool HasSameNameInCallStack(string functionName)
        {
            return callSet.Contains(functionName);
        }

        public void AddNamedEntity(string name, IEntityMeta meta = null)
        {
            namedEntities[name] = meta;
            scopes.Peek().Add(name);
        }

        public bool ContainsNamedEntity(string name)
        {
            return namedEntities.ContainsKey(name);
        }

        public bool TryGetNamedEntity<T>(string name, out T meta)
            where T : class, IEntityMeta
        {
            meta = null;
            if (namedEntities.TryGetValue(name, out var unknownMeta))
                meta = unknownMeta as T;

            return meta != null;
        }
    }
}