﻿using System.Collections.Generic;

namespace JBTestAssignment.Internal.States
{
    internal class VariablesContext
    {
        public IEnumerable<string> UsedCaptures => usedCaptures;
        public IEnumerable<string> AssignedCaptures => assignedCaptures;

        private readonly HashSet<string> assignedCaptures;
        private readonly HashSet<string> usedCaptures;
        private readonly HashSet<string> locals;

        public VariablesContext()
        {
            assignedCaptures = new HashSet<string>();
            usedCaptures = new HashSet<string>();
            locals = new HashSet<string>();
        }

        public void AddCapturedVariableUsage(string variableName)
        {
            usedCaptures.Add(variableName);
        }

        public void AddCapturedVariableAssignment(string variableName)
        {
            assignedCaptures.Add(variableName);
        }

        public bool HasLocal(string variableName)
        {
            return locals.Contains(variableName);
        }

        public bool ContainsAssignmentOf(string variableName)
        {
            return assignedCaptures.Contains(variableName);
        }

        public void AddLocal(string variableName)
        {
            locals.Add(variableName);
        }
    }
}