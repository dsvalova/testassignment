﻿namespace JBTestAssignment.Internal.Transformers
{
    internal interface INameResolver
    {
        string Resolve(string alias);
    }
}