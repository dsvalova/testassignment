﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBTestAssignment.Internal.EntityMetas;
using JBTestAssignment.Internal.States;
using JBTestAssignment.Statements;

namespace JBTestAssignment.Internal.Transformers
{
    /// <summary>
    /// Renames variables and functions in the expression. Used to avoid name collisions
    /// </summary>
    internal class NameCollisionsResolver : ITransformer, INameResolver
    {
        private readonly Func<string, string> newNameGenerator;
        private readonly Dictionary<string, string> aliasToName;
        private readonly AnalyzerState state;

        public NameCollisionsResolver() : this(GetNewName) { }

        public NameCollisionsResolver(Func<string, string> newNameGenerator)
        {
            this.newNameGenerator = newNameGenerator;
            aliasToName = new Dictionary<string, string>();
            state = new AnalyzerState();
        }

        public string Resolve(string alias)
        {
            if (aliasToName.TryGetValue(alias, out var name))
                return name;

            throw new ArgumentException($"Alias '{alias}' doesn't exist.");
        }

        public Program Transform(Program program)
        {
            var transformed = this.VisitAsEntryPoint(program);
            return transformed
                .OfType<FunctionDeclaration>()
                .First()
                .FunctionBody;
        }

        public IStatement Visit(VariableDeclaration statement)
        {
            return ToAliasNameInStatement(statement.VariableName, name => new VariableDeclaration(name));
        }

        public IStatement Visit(AssignVariable statement)
        {
            return ToAliasNameInStatement(statement.VariableName, name => new AssignVariable(name));
        }

        public IStatement Visit(PrintVariable statement)
        {
            return ToAliasNameInStatement(statement.VariableName, name => new PrintVariable(name));
        }

        public IStatement Visit(FunctionDeclaration functionsDeclaration)
        {
            string alias;
            if (state.TryGetNamedEntity<AliasEntity>(functionsDeclaration.FunctionName, out var aliasEntity))
            {
                alias = aliasEntity.Alias;
            }
            else
            {
                alias = newNameGenerator(functionsDeclaration.FunctionName);
                state.AddNamedEntity(functionsDeclaration.FunctionName, new AliasEntity(alias));
            }

            state.EnterFunction(functionsDeclaration.FunctionName);

            foreach (var statement in functionsDeclaration.FunctionBody)
            {
                if (statement is FunctionDeclaration localFunction && !state.ContainsNamedEntity(localFunction.FunctionName))
                    state.AddNamedEntity(localFunction.FunctionName, new AliasEntity(newNameGenerator(localFunction.FunctionName)));
            }

            var newBody = functionsDeclaration.FunctionBody
                .Select(statement => statement.Accept(this))
                .ToList();

            state.ExitFunction(functionsDeclaration.FunctionName);
            return new FunctionDeclaration(alias, new Program(newBody));
        }

        public IStatement Visit(Invocation statement)
        {
            return ToAliasNameInStatement(statement.FunctionName, name => new Invocation(name, statement.IsConditional));
        }

        private IStatement ToAliasNameInStatement(string name, Func<string, IStatement> newStatement)
        {
            if (state.TryGetNamedEntity<AliasEntity>(name, out var alias))
                return newStatement(alias.Alias);

            var newAlias = newNameGenerator(name);
            state.AddNamedEntity(name, new AliasEntity(newAlias));
            aliasToName[newAlias] = name;
            return newStatement(newAlias);
        }

        private static string GetNewName(string name)
        {
            return $"{name}_{Guid.NewGuid()}";
        }
    }
}
