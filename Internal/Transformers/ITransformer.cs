﻿using JBTestAssignment.Internal.Visitors;
using JBTestAssignment.Statements;

namespace JBTestAssignment.Internal.Transformers
{
    internal interface ITransformer : IVisitor<IStatement>
    {
        Program Transform(Program program);
    }
}