﻿using System;
using System.Collections.Generic;
using System.Linq;
using JBTestAssignment.Problems;
using JBTestAssignment.Internal.Visitors;
using JBTestAssignment.Statements;

namespace JBTestAssignment.Internal
{
    internal static class Extensions
    {
        internal static (List<IStatement> Accepted, List<IStatement> Others) Split<TAcceptable>(this List<IStatement> statements)
            where  TAcceptable : IStatement
        {
            var accepted = new List<IStatement>();
            var others = new List<IStatement>();
            foreach (var statement in statements)
            {
                if (statement is TAcceptable nextAccepted)
                    accepted.Add(nextAccepted);
                else
                    others.Add(statement);
            }

            return (accepted, others);
        }

        internal static List<IProblem> VisitEach(this IVisitor<List<IProblem>> visitor, IEnumerable<IStatement> statements)
        {
            return statements.SelectMany(statement => statement.Accept(visitor)).ToList();
        }

        internal static List<T> VisitEach<T>(this IVisitor<T> visitor, IEnumerable<IStatement> statements)
        {
            return statements.Select(statement => statement.Accept(visitor)).ToList();
        }

        internal static List<T> VisitAsEntryPoint<T>(this IVisitor<T> visitor, Program program)
        {
            return visitor.VisitEach(program.ToEntryPoint());
        }

        internal static List<IProblem> VisitAsEntryPoint(this IVisitor<List<IProblem>> visitor, Program program)
        {
            return visitor.VisitEach(program.ToEntryPoint());
        }

        private static Program ToEntryPoint(this Program program)
        {
            var entryPointName = $"EntryPoint{Guid.NewGuid()}";
            var entryPoint = new FunctionDeclaration(entryPointName, program);
            var entryPointInvocation = new Invocation(entryPointName, isConditional: false);
            return new Program {entryPoint, entryPointInvocation};
        }
    }
}
