﻿using System.Collections.Generic;
using JBTestAssignment.Problems;

namespace JBTestAssignment.Internal
{
    internal interface IAnalyzer
    {
        List<IProblem> Analyze(Program program);
    }
}