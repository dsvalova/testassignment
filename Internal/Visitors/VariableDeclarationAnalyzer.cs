﻿using System.Collections.Generic;
using JBTestAssignment.Internal.States;
using JBTestAssignment.Problems;
using JBTestAssignment.Statements;

namespace JBTestAssignment.Internal.Visitors
{
    /// <summary>
    /// Checks that something is printed or assigned if it's declared as a variable before.
    /// </summary>
    internal class VariableDeclarationAnalyzer : IVisitor<List<IProblem>>, IAnalyzer
    {
        private readonly AnalyzerState state = new AnalyzerState();

        public List<IProblem> Analyze(Program program)
        {
            return this.VisitAsEntryPoint(program);
        }

        public List<IProblem> Visit(VariableDeclaration statement)
        {
            if(!state.ContainsNamedEntity(statement.VariableName))
                state.AddNamedEntity(statement.VariableName);
            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(AssignVariable statement)
        {
            return CheckDeclaration(statement.VariableName);
        }

        public List<IProblem> Visit(PrintVariable statement)
        {
            return CheckDeclaration(statement.VariableName);
        }

        public List<IProblem> Visit(FunctionDeclaration declaration)
        {
            state.EnterFunction(declaration.FunctionName);
            var problems = this.VisitEach(declaration.FunctionBody);
            state.ExitFunction(declaration.FunctionName);
            return problems;
        }

        public List<IProblem> Visit(Invocation statement)
        {
            return AnalyzerResult.Successful;
        }

        private List<IProblem> CheckDeclaration(string variableName)
        {
            return !state.ContainsNamedEntity(variableName)
                ? new List<IProblem> { new UndeclaredVariableUsage(variableName) }
                : AnalyzerResult.Successful;
        }
    }
}