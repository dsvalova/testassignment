﻿using System.Collections.Generic;
using System.Linq;
using JBTestAssignment.Internal.States;
using JBTestAssignment.Problems;
using JBTestAssignment.Statements;

namespace JBTestAssignment.Internal.Visitors
{
    /// <summary>
    /// Checks that all names in scope are unique.
    /// </summary>
    internal class NamesUniquenessAnalyzer : IVisitor<List<IProblem>>, IAnalyzer
    {
        private readonly AnalyzerState state = new AnalyzerState();

        public List<IProblem> Analyze(Program program)
        {
            return this.VisitAsEntryPoint(program);
        }

        public List<IProblem> Visit(VariableDeclaration statement)
        {
            if (state.ContainsNamedEntity(statement.VariableName))
                return new List<IProblem> { new NameIsAlreadyDefined(statement.VariableName) };

            state.AddNamedEntity(statement.VariableName);
            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(AssignVariable statement)
        {
            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(PrintVariable statement)
        {
            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(FunctionDeclaration functionsDeclaration)
        {
            if (state.ContainsNamedEntity(functionsDeclaration.FunctionName))
                return new List<IProblem> { new NameIsAlreadyDefined(functionsDeclaration.FunctionName) };

            state.AddNamedEntity(functionsDeclaration.FunctionName);

            state.EnterFunction(functionsDeclaration.FunctionName);

            var (functionsDeclarations, otherStatements) = functionsDeclaration.FunctionBody.Split<FunctionDeclaration>();
            var problems = this.VisitEach(otherStatements.Concat(functionsDeclarations));

            state.ExitFunction(functionsDeclaration.FunctionName);
            return problems;
        }

        public List<IProblem> Visit(Invocation statement)
        {
            return AnalyzerResult.Successful;
        }
    }
}