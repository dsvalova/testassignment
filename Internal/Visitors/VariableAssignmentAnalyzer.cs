﻿using System.Collections.Generic;
using System.Linq;
using JBTestAssignment.Problems;
using JBTestAssignment.Internal.EntityMetas;
using JBTestAssignment.Internal.States;
using JBTestAssignment.Internal.Transformers;
using JBTestAssignment.Statements;

namespace JBTestAssignment.Internal.Visitors
{
    /// <summary>
    /// Checks that only assigned variables are printed.
    /// </summary>
    internal class VariableAssignmentAnalyzer : IVisitor<List<IProblem>>, IAnalyzer
    {
        private readonly VariableAssignmentAnalyzerState state;
        private readonly ITransformer nameUnifier;
        private readonly INameResolver nameResolver;

        public VariableAssignmentAnalyzer()
        {
            var alphaConverter = new NameCollisionsResolver();
            nameUnifier = alphaConverter;
            nameResolver = alphaConverter;

            state = new VariableAssignmentAnalyzerState();
        }

        public List<IProblem> Analyze(Program program)
        {
            var newProgram = nameUnifier.Transform(program);
            return this.VisitAsEntryPoint(newProgram);
        }

        public List<IProblem> Visit(VariableDeclaration statement)
        {
            if (!state.ContainsNamedEntity(statement.VariableName))
                state.AddVariable(statement.VariableName, new VariableMeta());

            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(AssignVariable statement)
        {
            state.AssignVariableIfExists(statement.VariableName);

            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(PrintVariable statement)
        {
            var isLocal = state.CurrentContext.HasLocal(statement.VariableName);
            if (state.TryGetVariable(statement.VariableName, out var variableMeta)
                && !variableMeta.IsAssigned
                && isLocal)
            {
                return new List<IProblem> { new UnassignedVariableUsage(nameResolver.Resolve(statement.VariableName)) };
            }

            if (!isLocal && !state.CurrentContext.ContainsAssignmentOf(statement.VariableName))
                state.CurrentContext.AddCapturedVariableUsage(statement.VariableName);

            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(FunctionDeclaration declaration)
        {
            if (!state.ContainsNamedEntity(declaration.FunctionName))
                state.AddFunction(declaration.FunctionName, new FunctionMeta { Body = declaration.FunctionBody });

            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(Invocation invocation)
        {
            if (!state.TryGetFunction(invocation.FunctionName, out var functionMeta))
                return AnalyzerResult.Successful;

            if (state.HasSameNameInCallStack(invocation.FunctionName))
                return AnalyzerResult.Successful;

            if (!functionMeta.HasBeenInvoked)
                Analyze(invocation.FunctionName, functionMeta);

            var assignmentAnalysis = functionMeta.AssignmentAnalysisResult;
            var problems = assignmentAnalysis.LocalProblems.ToList();
            problems.AddRange(assignmentAnalysis.UsedCapturedVariables
                .Where(variable => !IsAssigned(variable))
                .Select(variable => new UnassignedVariableUsage(nameResolver.Resolve(variable))));

            if (!invocation.IsConditional)
                AssignVariables(assignmentAnalysis.AssignedCapturedVariables);

            UseVariables(assignmentAnalysis.UsedCapturedVariables);

            return problems;
        }

        private void Analyze(string functionName, FunctionMeta functionMeta)
        {
            if (state.HasSameNameInCallStack(functionName))
                return;

            state.EnterFunction(functionName);

            var (functionsDeclarations, otherStatements) = functionMeta.Body.Split<FunctionDeclaration>();
            var localProblems = this.VisitEach(functionsDeclarations.Concat(otherStatements));

            functionMeta.AssignmentAnalysisResult = new AssignmentAnalysisResult(
                state.CurrentContext.AssignedCaptures.ToList(),
                state.CurrentContext.UsedCaptures.ToList(),
                localProblems);
            functionMeta.HasBeenInvoked = true;

            state.ExitFunction(functionName);
        }

        private bool IsAssigned(string variableName)
        {
            return !state.TryGetVariable(variableName, out var variableMeta) || variableMeta.IsAssigned;
        }

        private void UseVariables(IEnumerable<string> variables)
        {
            foreach (var name in variables)
            {
                if (!state.CurrentContext.HasLocal(name) && !state.CurrentContext.ContainsAssignmentOf(name))
                    state.CurrentContext.AddCapturedVariableUsage(name);
            }
        }

        private void AssignVariables(IEnumerable<string> variables)
        {
            foreach (var name in variables)
            {
                state.AssignVariableIfExists(name);
            }
        }
    }
}