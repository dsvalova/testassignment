﻿using System.Collections.Generic;
using JBTestAssignment.Internal.States;
using JBTestAssignment.Problems;
using JBTestAssignment.Statements;

namespace JBTestAssignment.Internal.Visitors
{
    /// <summary>
    /// Checks that something is called if it's declared as a function
    /// </summary>
    internal class FunctionDeclarationAnalyzer : IVisitor<List<IProblem>>, IAnalyzer
    {
        private readonly AnalyzerState state = new AnalyzerState();

        public List<IProblem> Analyze(Program program)
        {
            return this.VisitAsEntryPoint(program);
        }

        public List<IProblem> Visit(VariableDeclaration statement)
        {
            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(AssignVariable statement)
        {
            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(PrintVariable statement)
        {
            return AnalyzerResult.Successful;
        }

        public List<IProblem> Visit(FunctionDeclaration declaration)
        {
            if (!state.ContainsNamedEntity(declaration.FunctionName))
                state.AddNamedEntity(declaration.FunctionName);

            state.EnterFunction(declaration.FunctionName);

            foreach (var statement in declaration.FunctionBody)
            {
                if (statement is FunctionDeclaration localFunction)
                    state.AddNamedEntity(localFunction.FunctionName);
            }

            var problems = this.VisitEach(declaration.FunctionBody);

            state.ExitFunction(declaration.FunctionName);
            return problems;
        }

        public List<IProblem> Visit(Invocation invocation)
        {
            return !state.ContainsNamedEntity(invocation.FunctionName)
                ? new List<IProblem> { new UndeclaredFunctionUsage(invocation.FunctionName) }
                : AnalyzerResult.Successful;
        }
    }
}