﻿using JBTestAssignment.Statements;

namespace JBTestAssignment.Internal.Visitors
{
    public interface IVisitor<out T>
    {
        T Visit(VariableDeclaration statement);
        T Visit(AssignVariable statement);
        T Visit(PrintVariable statement);
        T Visit(FunctionDeclaration statement);
        T Visit(Invocation statement);
    }
}