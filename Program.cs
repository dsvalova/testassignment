﻿using System.Collections.Generic;
using System.Text;
using JBTestAssignment.Statements;

namespace JBTestAssignment
{
    public class Program : List<IStatement>
    {
        public Program() { }

        public Program(IEnumerable<IStatement> statements) : base(statements) { }

        public override string ToString()
        {
            var builder = new StringBuilder();

            foreach (var statement in this) builder.AppendLine(statement.ToString());

            return builder.ToString();
        }
    }
}